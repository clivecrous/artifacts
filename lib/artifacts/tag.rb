class Artifacts

  Artifact ||= Class.new( YAML_Model )
  class Tag < YAML_Model
    InvalidName = Class.new( Exception )
    VALID_NAME = '[-a-z]+'
    type :name, String do |value|
      raise InvalidName unless value =~ /^#{VALID_NAME}$/
    end
    has :artifacts, Artifact, :many_to_many => true
    init :name
    def to_s
      name
    end
    def inspect
      "#{to_s}: #{artifacts.map{|n|n.id}.join(',')}"
    end
    def <=>( other )
      self.name <=> other.name
    end
  end

end
