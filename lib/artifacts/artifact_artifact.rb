class Artifacts
  Artifact ||= Class.new( YAML_Model )
  class ArtifactArtifact < YAML_Model
    type :depended_on_by, Artifact
    type :depends_on, Artifact
    init :depended_on_by, :depends_on
  end
end
