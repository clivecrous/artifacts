require 'artifacts'

class Artifacts

  def self.username
    "#{`git config user.name`.strip} <#{`git config user.email`.strip}>"
  end

  class Executable

    def self.stub( method, default )
      define_method method do |*args|
        command = args.shift
        command ||= default
        send( "#{method}__#{command}".to_sym, *args )
      end
    end

    def initialize( args )
      if args.first == 'init'
        args.shift
        path = args.shift
        Artifacts.new( path )
        puts "Artifacts repository created at: #{path}"
        exit 0
      end
      begin
        Artifacts.new
      rescue Artifacts::UnableToLocatePath
        puts "Unable to locate artifacts data file!"
        raise
      end

      @closed = false
      @open = true
      @verbose = false
      @dependants = false

      unless args.select{|n|n=~/^(?:-c|--closed)$/}.empty?
        @closed = true
        @open = false
        args.delete( '-c' )
        args.delete( '--closed' )
      end

      unless args.select{|n|n=~/^(?:-a|--all)$/}.empty?
        @closed = true
        @open = true
        @dependants = true
        args.delete( '-a' )
        args.delete( '--all' )
      end

      unless args.select{|n|n=~/^(?:-v|--verbose)$/}.empty?
        @verbose = true
        args.delete( '-v' )
        args.delete( '--verbose' )
      end

      unless args.select{|n|n=~/^(?:-d|--dependants)$/}.empty?
        @dependants = true
        args.delete( '-d' )
        args.delete( '--dependants' )
      end

      unless args.select{|n|n=~/^(?:-l|--limit)$/}.empty?
        idx = args.index( '-l' )
        idx ||= args.index( '--limit' )
        args.delete_at( idx )
        @limit = args.delete_at( idx ).to_i
      end

      @limit = ( @verbose ? 3 : 10 ) if @limit.to_i < 1

      @artifacts = []
      @tags = []
      any_artifacts_given = false
      while match = args.first && args.first.match( /^(?:(\d+)|#(#{Tag::VALID_NAME}))$/ )
        any_artifacts_given = true
        if match[1]
          @artifacts << Artifact[ args.shift.to_i ]
        elsif match[2]
          args.shift
          @tags << match[2]
          tag = Tag.select{|n|n.name == match[2]}.first
          @artifacts << tag.artifacts if tag
        end
      end

      @artifacts.flatten!
      @artifacts.uniq!

      @tags.flatten!
      @tags.uniq!

      @artifacts = Artifact.all unless any_artifacts_given

      @artifacts = @artifacts.select do |artifact|
        ( ( @open && !artifact.closed ) ||
          ( @closed && artifact.closed ) ) &&
        ( @dependants || artifact.open_depends_on.size == 0 )
      end

      @artifacts.sort!

      command = args.shift
      command ||= :list

      send( command.to_sym, *args )
    end

    def method_missing( method, *args )
      puts "Unknown command: #{method.to_s.gsub('__',' ')}"
    end

    def help_command( command, description )
      puts( "%-10s : %s"%[command,description] )
    end

    def help__help( *args )
      help_command :help, 'Display this message. For more information on a specific command use `help <command>` eg: `help tag`'
    end

    def help__list( *args )
      help__help
      help_command :tag,  'Tagging'
      help_command :create,  'Create a new artifact'
      help_command :list, 'List artifacts'
    end

    def list
      @limit = @artifacts.size if @limit > @artifacts.size
      @limit.times do
        artifact = @artifacts.shift
        puts artifact.send( @verbose ? :inspect : :to_s )
      end
      puts "... #{@artifacts.size} more ..." unless @artifacts.empty?
    end

    def create( *args )
      artifact = Artifact.create( Artifacts.username, args.join(' ') )
      puts "Created artifact #{artifact.id}."
      @artifacts = [artifact]
      tag( *@tags )
    end

    def close
      @artifacts.each do |artifact|
        artifact.closed = Time.now
        puts "Closed: #{artifact}"
      end
    end

    def priority( priority )
      priority = priority.to_i
      @artifacts.each do |artifact|
        artifact.priority = priority
        puts "Priority changed to #{priority} for: #{artifact}"
      end
    end

    def version
      puts "\nArtifacts: #{Artifacts::VERSION}"
    end

    stub :help, :list

    def depends( *args )
      args.shift if args.first == 'on' # optional syntax sugar
      depends_on_list = args.map{|id|Artifact[id.to_i]}
      @artifacts.each do |artifact|
        depends_on_list.each do |depends_on|
          ArtifactArtifact.create( artifact, depends_on )
          puts "Artifact [#{artifact.id}] now depends on artifact [#{depends_on.id}]"
        end
      end
    end

    def tag( *args )
      args.each do |tag_name|
        tag = Tag.select{|n|n.name == tag_name}.first
        tag ||= Tag.create( tag_name )
        @artifacts.each do |artifact|
          puts "Adding tag \"#{tag.name}\" to #{artifact}"
          artifact.add_tag( tag )
        end
      end
    end

    def untag( *args )
      args.each do |tag_name|
        tag = Tag.select{|n|n.name == tag_name}.first
        @artifacts.each do |artifact|
          puts "Removing tag \"#{tag.name}\" from #{artifact}"
          artifact.remove_tag( tag )
        end
      end
    end

    def prune
      Tag.all.sort{|a,b|a.name <=> b.name}.each do |tag|
        if tag.artifacts.size == 0
          puts "Pruning tag: #{tag.name}"
          tag.delete
        end
      end
    end

    def tags( *args )
      @artifacts.map{|n|n.tags}.flatten.uniq.sort.each do |tag|
        puts "#{tag}: #{@artifacts.select{|n|n.tags.index(tag)}.map{|n|n.id.to_s}.join(',')}"
      end
    end

    def comment( *args )
      text = args.join(' ')
      author = Artifacts.username
      @artifacts.each do |artifact|
        Comment.create( artifact, author, text )
      end
      puts "Comments added to:"
      list
    end

    def tag__create( tag )
      Tag.create( tag )
    end

  end
end
