class Artifacts

  Artifact ||= Class.new( YAML_Model )
  class Comment < YAML_Model
    type :artifact, Artifact
    type :created, Time, :default => Time.now
    type :author, String
    type :text, String

    init :artifact, :author, :text

    def to_s
      spaces = ' '*(YAML_Model.next_oid.to_s.length+1)
      "#{spaces}#{created.strftime('%Y-%m-%d')} #{author}\n#{spaces}#{text}"
    end

    def inspect
      to_s
    end

  end

end

