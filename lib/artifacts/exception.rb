class Artifacts
  Exception = Class.new( ::Exception )
  %w|UnableToLocatePath|.each do |exception|
    class_eval( "#{exception} = Class.new( Exception )" )
  end
end
