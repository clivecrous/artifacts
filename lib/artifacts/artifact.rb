class Artifacts

  Comment ||= Class.new( YAML_Model )
  Tag ||= Class.new( YAML_Model )
  ArtifactArtifact ||= Class.new( YAML_Model )
  class Artifact < YAML_Model
    type :creator, String
    type :summary, String

    type :priority, Integer, :default => 0

    type :created, Time, :default => Time.now
    type :closed, [NilClass,Time], :default => nil

    has :depended_on_by_references, ArtifactArtifact, ArtifactArtifact => :depended_on_by
    has :depends_on_references, ArtifactArtifact, ArtifactArtifact => :depends_on

    has :tags, Tag, :many_to_many => true
    has :comments, Comment
    init :creator, :summary

    def self.priority_width
      all.map{|n|n.priority.to_s}.sort{|a,b|a.length <=> b.length}.last.length
    end

    def open?
      closed == nil
    end

    def closed?
      closed != nil
    end

    def <=>( other )
      offset = ( closed? ^ other.closed? ) ? ( closed? ? 1 : -1 ) : 0
      offset = other.priority <=> self.priority if offset == 0
      offset = other.open_depended_on_by.size <=> self.open_depended_on_by.size if offset == 0
      offset = self.id <=> other.id if offset == 0
      offset
    end

    def open_depends_on
      depends_on.select{|n|!n.closed}
    end

    def depends_on
      depended_on_by_references.map{|n|n.depends_on}
    end

    def open_depended_on_by
      depended_on_by.select{|n|!n.closed}
    end

    def depended_on_by
      depends_on_references.map{|n|n.depended_on_by}
    end

    def to_s
      "%#{YAML_Model.next_oid.to_s.length}d #{depended_on_by.size > 0 ? 'd' : '-'}#{depends_on.size > 0 ? 'D' : '-'}#{comments.size > 0 ? 'c' : '-'} %#{self.class.priority_width}s #{summary}"%[id,closed ? 'X' : ( priority == 0 ? '' : priority.to_s )]
    end

    def inspect
      indentation = ' '*(YAML_Model.next_oid.to_s.length + 1)
      "#{to_s}\n" +
                                    indentation + 'created        : ' + created.to_s + "\n" +
      ( closed                   ? (indentation + 'closed         : ' + closed.to_s + "\n" ) : '' ) +
      ( tags.size  > 0           ? (indentation + 'tags           : [' + tags.map{|n|n.name}.join(',') + "]\n" ) : '' ) +
      ( depended_on_by.size  > 0 ? (indentation + 'depended on by : [' + depended_on_by.map{|n|n.id}.join(',') + "]\n" ) : '' ) +
      ( depends_on.size > 0      ? (indentation + 'depends on     : [' + depends_on.map{|n|n.id}.join(',') + "]\n" ) : '' ) +
      ( comments.size > 0        ? (indentation + "comments       :\n" + comments.join("\n")) : '' )
    end

    def priority
      open_depended_on_by.inject( @priority ) do |priority,artifact|
        priority += artifact.priority
      end
    end

  end

end
