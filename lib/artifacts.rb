require 'yaml-model'

require 'artifacts/version'
require 'artifacts/exception'

require 'artifacts/artifact'
require 'artifacts/artifact_artifact'
require 'artifacts/tag'
require 'artifacts/comment'

class Artifacts

  FILENAME = '.artifacts'

  def self.guess_path
    directory = Dir.pwd
    until directory.empty? do
      path = File.join( directory, FILENAME )
      return directory if File.exists?( path )
      previous_directory = directory
      directory = File.join( File.split( directory )[0..-2] )
      break if directory == previous_directory
    end
    raise UnableToLocatePath
  end

  def initialize( path = self.class.guess_path )
    YAML_Model.filename = File.join( path, FILENAME )
  end

end
